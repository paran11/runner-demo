FROM python:3.8.0-slim
WORKDIR /app
ADD . /app
ENV NAME Paran
CMD ["python", "app.py"]